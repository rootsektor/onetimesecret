import sqlite3
import random
import string
import cryptocode


class Secret:
    length = 32
    secret = None
    key = None
    debug = False

    def __init__(self, secret, key):
        self.secret = secret
        self.key = key

        connection = sqlite3.connect("secrets.db")
        cursor = connection.cursor()

        try:
            cursor.execute("CREATE TABLE secrets (secret TEXT, link TEXT, datetime DATETIME DEFAULT CURRENT_TIMESTAMP)")
        except Exception as e:
            if(self.debug):
                print(str(e))
            pass

        self.delete_old_entries()


    def get_random_string(self):
        letters = string.ascii_letters + string.digits
        rand_str = ''.join(random.choice(letters) for i in range(self.length))
        return rand_str


    def delete_old_entries(self):
        connection = sqlite3.connect("secrets.db")
        cursor = connection.cursor()

        cursor.execute("DELETE FROM secrets WHERE datetime <= date('now', '-7 day')")
        connection.commit()


    def insert(self):
        connection = sqlite3.connect("secrets.db")
        cursor = connection.cursor()

        rand_str = self.get_random_string()
        secret_enc = self.encrypt(self.secret)
        try:
            cursor.execute("INSERT INTO secrets VALUES ('" + secret_enc + "', '" + rand_str + "', CURRENT_TIMESTAMP)")
        except Exception as e:
            if(self.debug):
                print(str(e))
            pass

        connection.commit()

        return rand_str


    def read(self, link):
        connection = sqlite3.connect("secrets.db")
        cursor = connection.cursor()

        data = cursor.execute("SELECT secret FROM secrets WHERE link = '" + link + "'").fetchone()
        cursor.execute("DELETE FROM secrets WHERE link = '" + link + "'")
        connection.commit()

        return self.decrypt(data[0])


    def encrypt(self, message):
        encrypted = cryptocode.encrypt(message, self.key)
        return encrypted


    def decrypt(self, message):
        decrypted = cryptocode.decrypt(message, self.key)
        return decrypted
