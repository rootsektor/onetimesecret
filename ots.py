import jinja2
import configparser
from classes.secret import Secret
from flask import *

version = '0.1'
app = Flask(__name__)
template_dir = './templates'
loader = jinja2.FileSystemLoader(template_dir)
environment = jinja2.Environment(loader=loader)
config = configparser.ConfigParser()
config.readfp(open(r'config.txt'))
key = config.get('Main', 'key')
base_url = config.get('Main', 'base_url')
app.config['SECRET_KEY'] = key


@app.route('/', methods=['GET'])
def index():
    return render_template('index.html', version = version)


@app.route('/create', methods=['POST'])
def create():
    message = request.form.get('secret')
    secret = Secret(message, key)
    link = secret.insert()
    
    return render_template('created.html', link = link, base_url = base_url)


@app.route('/secret/<link>', methods=['GET'])
def read(link):
    secret = Secret(None, key)
    try:
        secret = secret.read(link)
        return render_template('read.html', secret = secret, version = version)
    except:
        flash('Secret not found!', 'danger')
        return redirect(url_for('index'))


@app.route('/burn', methods=['POST'])
def burn():
    link = request.form.get('link')
    try:
        secret = Secret(None, key)
        secret.read(link)
        flash('Secret burned...', 'success')
    except:
        flash('Failed burning secret...', 'danger')
        pass
    
    return redirect(url_for('index'))
